import { Button, Container, Nav, Navbar as NavbarBs } from 'react-bootstrap'
import { NavLink } from 'react-router-dom'
import { useShoppingCart } from '../context/ShoppingCartContext'

function Navbar() {
  const { openCart, cartQuantity } = useShoppingCart()
  return (
    <NavbarBs className='bg-white shadow-sm mb-3'>
      <Container>
        <Nav className='me-auto'>
          <Nav.Link to='/' as={NavLink}>Home</Nav.Link>
          <Nav.Link to='/store' as={NavLink}>Store</Nav.Link>
          <Nav.Link to='/about' as={NavLink}>About</Nav.Link>
        </Nav>
        <Button onClick={openCart} variant='outline-primary' className=''>CART
          {cartQuantity > 0 && <p className='rounded-circle bg-danger text-white d-inline-block mb-0' style={{ width: "1.5rem", height: '1.5rem' }}>{cartQuantity}</p>}
        </Button>
      </Container>
    </NavbarBs>
  )
}

export default Navbar