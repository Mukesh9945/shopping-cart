import React from 'react'
import { Button, Stack } from 'react-bootstrap'
import { useShoppingCart } from '../context/ShoppingCartContext'
import storeItems from '../data/items.json'
import { formatCurrency } from '../utilities/formatCurrency'

type CartItemProps = {
    id: number
    quantity: number
}
function CartItem({ id, quantity }: CartItemProps) {
    const { removeFromCart } = useShoppingCart() 
    const item = storeItems.find(i => i.id === id)
    if (item === null) return null
    return (
        <Stack direction='horizontal' gap={2} className="d-flex align-items-center">
            <img src={item?.imgUrl} style={{ width: '125px', height: '75px', objectFit: 'cover' }} />
            <div className='me-auto d-flex  align-items-center justify-space-between'>
                <div>
                    {item?.name} {quantity > 1 && <span className='text-muted' style={{fontSize:'.65rem'}}>x{quantity}</span>}
                    {/* <div className="text-muted" style={{fontSize:'.65rem'}}>{formatCurrency(item.price)}</div> */}
                </div>
                <div className='d-flex align-items-center'>
                    {/* <div>{formatCurrency(item.price * quantity)}</div> */}
                    <Button onClick={() => removeFromCart(id)} variant='danger' size='sm' className='text-white'>X</Button>
                </div>
            </div>
        </Stack>
    )
}

export default CartItem